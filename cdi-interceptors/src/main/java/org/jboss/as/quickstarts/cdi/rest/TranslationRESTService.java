package org.jboss.as.quickstarts.cdi.rest;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.as.quickstarts.cdi.service.Translator;
import org.jboss.as.quickstarts.cdi.util.Language;
import org.jboss.as.quickstarts.cdi.util.TranslatorProducer;
import org.jboss.as.quickstarts.cdi.util.Language.LANGUAGE;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service that translates hi depending on
 * implementation.
 * 
 */
@Path("/translation")
@RequestScoped
public class TranslationRESTService {

    private static final Logger LOGGER = Logger.getLogger(TranslationRESTService.class.getName());

    @Inject
    private Translator translator;

    @Inject
    @Language(LANGUAGE.SWE)
    private Translator translator2;

    @GET
    @Path("/ping")
    @Produces(MediaType.TEXT_PLAIN)
    public Response ping() {
        LOGGER.info(" translator1:" + translator.toString() + " translator2:" + translator2.sayHello() + "¤"
                + translator2.toString());
        return Response.status(Response.Status.OK)
                .entity("pong translator1:" + translator.toString() + " translator2:" + translator2.toString()).build();
    }

    @GET
    @Path("/sayHi")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getTranslation() {
        return Response.status(Response.Status.OK).entity(translator.sayHello()).build();

    }
}
