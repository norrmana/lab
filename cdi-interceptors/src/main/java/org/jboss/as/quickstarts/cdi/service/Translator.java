package org.jboss.as.quickstarts.cdi.service;

public interface Translator {
    String sayHello();
}