package org.jboss.as.quickstarts.cdi.serviceImpl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.cdi.service.Translator;

/**
 * TBD
 *
 */
@Named("translatorbean")
@RequestScoped
public class TranslatorBean {

    @Inject
    private Translator translator;

    public String getSayHi() {
        return translator.sayHello();
    }
}
