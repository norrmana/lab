package org.jboss.as.quickstarts.cdi.serviceImpl;

import java.io.Serializable;

import org.jboss.as.quickstarts.cdi.service.Translator;
import org.jboss.as.quickstarts.cdi.util.English;
import org.jboss.as.quickstarts.cdi.util.Language;

@English
@Language(Language.LANGUAGE.ENG)
public class TranslatorBeanEN implements Translator, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TranslatorBeanEN() {
    }

    @Override
    public String sayHello() {
        return "Hello!";
    }

}
