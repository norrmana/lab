package org.jboss.as.quickstarts.cdi.serviceImpl;

import java.io.Serializable;

import org.jboss.as.quickstarts.cdi.service.Translator;
import org.jboss.as.quickstarts.cdi.util.Language;
import org.jboss.as.quickstarts.cdi.util.Swedish;

@Swedish
@Language(Language.LANGUAGE.SWE)
public class TranslatorBeanSE implements Translator, Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TranslatorBeanSE() {
    }

    @Override
    public String sayHello() {
        return "Hej!";
    }

}
