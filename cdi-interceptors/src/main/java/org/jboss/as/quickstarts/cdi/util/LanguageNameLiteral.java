package org.jboss.as.quickstarts.cdi.util;

import javax.enterprise.util.AnnotationLiteral;

public class LanguageNameLiteral extends AnnotationLiteral<Language> implements Language {

    private static final long serialVersionUID = 1L;
    final Language.LANGUAGE language;

    LanguageNameLiteral(Language.LANGUAGE eng) {
        this.language = eng;
    }

    @Override
    public Language.LANGUAGE value() {
        return language;
    }

}
