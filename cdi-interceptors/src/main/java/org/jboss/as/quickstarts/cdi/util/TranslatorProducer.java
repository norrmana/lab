package org.jboss.as.quickstarts.cdi.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

import org.jboss.as.quickstarts.cdi.service.Translator;
import org.jboss.as.quickstarts.cdi.util.Language.LANGUAGE;

/**
 * A producer method must be a non-abstract method of a managed bean class or
 * session bean class. A producer method may be either static or non-static. If
 * the bean is a session bean, the producer method must be either a business
 * method of the EJB or a static method of the bean class
 *
 *
 */
public class TranslatorProducer {

    private static final Logger LOGGER = Logger.getLogger(TranslatorProducer.class.getName());

    @Inject
    @English
    private Translator translatorEN;

    @Inject
    @Swedish
    @Language(LANGUAGE.SWE)
    private Translator translatorSE;

    @Inject
    @Any
    private Instance<Translator> translations;

    @PostConstruct
    private void post() {
        LOGGER.info("****************************************************");
        LOGGER.info("Translations ENG:" + translatorEN.sayHello() + " SWE:" + translatorSE.sayHello());
        LOGGER.info("****************************************************");
    }

    private Translator getPredicated(String predicate) {
        return translations.select(new AnnotationLiteral<Swedish>() {
        }).get();

    }

    /**
     * The scope of the producer method defaults to @Dependent, and so it will
     * be called every time the container injects this field or any other field
     * that resolves to the same producer method. Thus, there could be multiple
     * instances of the PaymentStrategy object for each user session.
     * 
     * See
     * https://docs.jboss.org/weld/reference/1.0.0/en-US/html/producermethods.html#d0e3306
     * 
     * This behavior could be changed by changing the scope of the producer.
     * Comment out below to see.
     */
    @Produces
    // @ApplicationScoped
    // @SessionScoped
    // @RequestScoped
    public Translator getTranslator() {
        LOGGER.info("Producer: " + this.toString());
        if (!translations.isUnsatisfied()) {
            Translator translator = null;
            if (Calendar.getInstance().get(Calendar.SECOND) % 2 == 0) {
                translator = translations.select(new LanguageNameLiteral(Language.LANGUAGE.ENG)).get();
            } else {
                translator = translations.select(new LanguageNameLiteral(Language.LANGUAGE.SWE)).get();
            }
            return translator;
        } else {
            throw new RuntimeException("Translation injection failed");
        }
    }

}
